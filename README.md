# happypath - results

- [gitlab](https://gitlab.com/happypath/results)
- ila9208 - C:\@COM\gitlab\happypath\results

## The happy path

- the 20% of work to accomplish 80% of the task
- rapid prototypes with possibility to add detail handling as needed
- simplification is not loss

## Concepts

- Maybe
  - data <> nothing

- Either
  - right <> left
  - success <> failure

-

## Isomorphism

not equal but contain basically the same information

- Lazyness
  \
  data and a function producing that data are isomorphic
  ```javascript
  const data = {//...}
  const lazyData = () => data
  const copy = lazyData()
  ```

## Handling strategies

- if-then-else
- try-catch
