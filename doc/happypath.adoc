= Happy Path
ga - {docdate}

:wb: whiteboard
== On the {wb}
First prototypes or Proof of Concepts (PoC) are easyly sketched on the {wb}.
When it comes to an implementation of these ideas, the handling of edge cases starts playing an increasingly important role:

* the programmer's experience 'rings bells' on seeing certain patterns
* return values of api's are not 'so' simple
* the typechecker complains about 'unhandled cases'

On the {wb} we 'abstract away' these details, and thus have a clearer image of the important parts.
====
we sketch the "Happy Path", an idealized workflow.
====

== Ofuscation by coding

The programmers strategies for handling the non-ideal flows are multiple

:ite: if-then-else
=== `{ite}` 
* inputs are checked before doing things
* outputs need checking before doing the next step
* long chains of actions entail deeply nested {ite}s

=== `swich`
* just an other {ite}...
* pitfalls (passthru cases, missing default handler)

=== `try-catch-finally`
* things go wrong unexpectedly
* mis-use of exceptions as flow control
* resource releasing in `finally` block`
* re-throwing exceptions 

All these strategies end up in code that is:

* repetitive (boilerplate)
* difficult to follow/debug
* mixing concerns

====
The more we code, the more we hide the "Happy Path" 
====
