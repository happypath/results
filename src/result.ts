import type { Types as T } from "./deps.ts";
import { NonEmptyArray } from "./Types.ts";

type Option<A> = T.Option<A>;
type OptionMatcher<A, B> = T.OptionMatcher<A, B>;
type Just<A> = T.Just<A>;
type Nil = T.Nil;
type FnAB<A, B> = T.FnAB<A, B>;
type FnABC<A, B, C> = T.FnABC<A, B, C>;
type FnABCu<A, B, C> = T.FnABCu<A, B, C>;

const NIL = { t: "Nil" };

// constructors
const nil = () => NIL as Option<never>;
const just = <A>(v: A) => ({ t: "Just", v }) as Option<A>;

const equalsOption = <A extends B, B>(oa: Option<A>) => (ob: Option<B>) =>
  (oa.t === ob.t) && (oa.t === "Nil" || oa.v === (ob as Just<A>).v);

// Foldable
const foldOption = <A, B>(matcher: OptionMatcher<A, B>) => (oa: Option<A>) =>
  oa.t === "Nil" ? matcher.onNil() : matcher.onJust(oa.v);

const mapOption = <A, B>(fn: FnAB<A, B>) => (oa: Option<A>) =>
  oa.t === "Nil" ? oa : just(fn(oa.v));

const apOption = <A, B>(ofn: Option<FnAB<A, B>>) => (oa: Option<A>) =>
  (ofn.t === "Just" && oa.t === "Just") ? just(ofn.v(oa.v)) : NIL as Option<B>;

const lifta2Option =
  <A, B, C>(fn: FnABC<A, B, C>) => (oa: Option<A>) => (ob: Option<B>) =>
    (oa.t === "Just" && ob.t === "Just")
      ? just(fn(oa.v)(ob.v))
      : NIL as Option<C>;

const lifta2Optionu =
  <A, B, C>(fn2u: FnABCu<A, B, C>) => (oa: Option<A>, ob: Option<B>) =>
    (oa.t === "Just" && ob.t === "Just")
      ? just(fn2u(oa.v, ob.v))
      : NIL as Option<C>;

/*
type ONea<A> = Option<NonEmptyArray<A>>;
const sequence = <A>(aos: NonEmptyArray<Option<A>>): Option<NonEmptyArray<A>> =>
  aos.reduce(
    (s, o) =>
      (o.t !== "Nil" && s.t !== "Nil" ? just(s.v.concat(o.v)) : NIL) as ONea<A>,
    just([]) as ONea<A>,
  );
*/
const sequence = <A>(aos: Array<Option<A>>): Option<Array<A>> =>
  aos.reduce(
    (s, o) =>
      (o.t !== "Nil" && s.t !== "Nil" ? just(s.v.concat(o.v)) : NIL) as Option<
        Array<A>
      >,
    just([]) as Option<Array<A>>,
  );

export const Result = {
  // construct
  nil,
  just,
  //
  Eq: {
    equals: equalsOption,
  },
  //
  fold: foldOption,
  //
  map: mapOption,
  //
  ap: apOption,
  lifta2: lifta2Option,
  lifta2u: lifta2Optionu,
  sequence,
};
