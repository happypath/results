//// Function
//
export type FnLazy<L> = () => L;
//
export type FnAB<A, B> = (a: A) => B;
export type FnABC<A, B, C> = (a: A) => (b: B) => C;
export type FnABCu<A, B, C> = (a: A, b: B) => C;
//
export type Op<A> = (a1: A) => (a2: A) => A;
export type Opu<A> = (a1: A, a2: A) => A;
//
export interface Nil {
  t: "Nil";
}
export interface Just<A> {
  t: "Just";
  v: A;
}
export type Option<A> = Nil | Just<A>;
export interface OptionMatcher<A, B> {
  onNil: FnLazy<B>;
  onJust: FnAB<A, B>;
}

// Array
export type NonEmptyArray<A> = [a1: A, ...as: A[]];
