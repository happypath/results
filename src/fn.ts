import { FnABC } from "./Types.ts";

export function identity<A>(a: A) {
  return a;
}

//// number - curried/uncurried
// associtive / commutative
export function add(increment: number) {
  return (a: number) => a + increment;
}
export function addu(a: number, increment: number) {
  return a + increment;
}
export function mult(factor: number) {
  return (a: number) => a * factor;
}
export function multu(a: number, factor: number) {
  return a * factor;
}
// not associtive / not commutative
export function substract(decrement: number) {
  return (a: number) => a - decrement;
}
export function substractu(a: number, decrement: number) {
  return a - decrement;
}
export function divideBy(divisor: number) {
  return (a: number) => a / divisor;
}
export function divideByu(a: number, divisor: number) {
  return a / divisor;
}

// Array
export const Arrays = {
  /** Array monoid\<A> - lazy empty */
  empty: <A>() => [] as A[],
  /** Array monoid\<A> - combine operator */
  combine: <A>(a1: A[]) => (a2: A[]) => Array.prototype.concat(a1, a2) as A[],

  /** Array foldl - fold left */
  foldl: <A, B>(fnbab: FnABC<B, A, B>) => (initB: B) => (as: A[]) => {
    return as.reduce(
      (b, a) => fnbab(b)(a),
      initB,
    );
  },
  /** Array foldr - fold right*/
  foldr: <A, B>(fnabb: FnABC<A, B, B>) => (initB: B) => (as: A[]) => {
    return as.reduceRight(
      (b, a) => fnabb(a)(b),
      initB,
    );
  },

  /** Array.concat - constrainted to unknown*/
  concat: (a1: unknown[], a2: unknown) =>
    Array.prototype.concat(a1, a2) as unknown[],
};
