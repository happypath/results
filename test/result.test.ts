import type { Option } from "../src/Types.ts";
import { asserts as A, Fn, Result as R } from "./test_deps.ts";

Deno.test("Option", async (t) => {
  //
  const n = R.nil();
  const j1 = R.just(1);
  const j2 = R.just(2);

  const equals = R.Eq.equals;
  // TODO use assertEquals with Show when defined (assertEquals compares real structure)
  const assertOEquals = <A, B>(a: Option<A>, b: Option<B>) =>
    A.assert(equals(a)(b));

  await t.step("constructors", () => {
    A.assertEquals(n.t, "Nil");
    A.assertEquals(j1.t, "Just");
    A.assertEquals(j1.t === "Just" && j1.v, 1);
  });

  await t.step("Eq", () => {
    assertOEquals(n, n);
    assertOEquals(R.nil(), R.nil());
    assertOEquals(j1, j1);
    assertOEquals(R.just(2), R.just(2));
    A.assert(!equals(n)(j1));
    A.assert(!equals(j1)(n));
  });

  await t.step("Foldable", () => {
    const getOrUndefined = R.fold({
      onNil: () => undefined,
      onJust: Fn.identity<number>,
    });

    A.assertEquals(getOrUndefined(n), undefined);
    A.assertEquals(getOrUndefined(j1), 1);
  });

  await t.step("Functor", () => {
    //
    const incrementO = R.map(Fn.add(1));
    const doubleO = R.map(Fn.mult(2));
    //
    assertOEquals(incrementO(j1), j2);
    assertOEquals(incrementO(n), n);
    assertOEquals(doubleO(j1), j2);
    assertOEquals(doubleO(n), n);
  });

  await t.step("Applicative Functor", async (t2) => {
    await t2.step("unary Fn - ap", () => {
      //
      const incrementO = R.ap(R.just(Fn.add(1)));
      const doubleO = R.ap(R.just(Fn.mult(2)));
      //
      assertOEquals(incrementO(j1), j2);
      assertOEquals(incrementO(n), n);
      assertOEquals(doubleO(j1), j2);
      assertOEquals(doubleO(n), n);
    });

    await t2.step("binary Fn - lifta2", () => {
      //
      const addO = R.lifta2(Fn.add);
      assertOEquals(addO(j1)(j1), j2);
      assertOEquals(addO(j1)(n), n);
      assertOEquals(addO(n)(j1), n);
      assertOEquals(addO(n)(n), n);
      //
      const addOu = R.lifta2u(Fn.addu);
      assertOEquals(addOu(j1, j1), j2);
      assertOEquals(addOu(j1, n), n);
      assertOEquals(addOu(n, j1), n);
      assertOEquals(addOu(n, n), n);

      //
      const substractO = R.lifta2(Fn.substract);
      const substractOj1 = substractO(j1);
      const substractOn = substractO(n);
      assertOEquals(substractOj1(j2), j1);
      assertOEquals(substractOj1(n), n);
      assertOEquals(substractOn(j1), n);
      assertOEquals(substractOn(n), n);
      //
      const substractOu = R.lifta2u(Fn.substractu);
      assertOEquals(substractOu(j2, j1), j1);
      assertOEquals(substractOu(j2, n), n);
      assertOEquals(substractOu(n, j1), n);
      assertOEquals(substractOu(n, n), n);
    });
    await t2.step("sequence", () => {
      //
      const ass = [[], [1], [1, 2]] as number[][];
      ass.forEach((as) =>
        A.assertEquals(R.sequence(as.map(R.just)), R.just(as))
      );
      A.assertEquals(R.sequence([n]), n);
      A.assertEquals(R.sequence([n, n]), n);
      A.assertEquals(R.sequence([n, j1]), n);
      A.assertEquals(R.sequence([j1, j2, n]), n);
    });
  });
});
