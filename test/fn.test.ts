import { asserts as A, Fn } from "./test_deps.ts";

Deno.test("Function", async (t) => {
  //
  await t.step("idenity", () => {
    const lits = [undefined, null, true, 1, "hello", { a: 1 }, [1]];
    lits.forEach((lit) => {
      A.assertEquals(Fn.identity(lit), lit);
    });
  });
});

Deno.test("Function - number", async (t) => {
  //
  await t.step("add", () => {
    const inc = Fn.add(1);
    A.assertEquals(inc(1), 2);
  });
  //
  await t.step("mult", () => {
    const double = Fn.mult(2);
    A.assertEquals(double(1), 2);
  });
});

Deno.test("Function - Arrays", async (ta) => {
  //
  const AR = Fn.Arrays;

  await ta.step("concat", () => {
    const a1 = [1, "2"];
    const a2 = [true, {}];
    const concat = AR.concat(a1, a2);
    A.assertEquals(concat.slice(0, 2), a1);
    A.assertEquals(concat.slice(2, 4), a2);

    const mixed = AR.concat(AR.empty<number>(), "2");
    A.assertEquals(mixed, ["2"]);
  });
  //
  await ta.step("monoid", () => {
    const a1 = [1, 2];
    const a2 = [3, 4];
    const a3 = [5, 6];

    A.assertEquals(AR.empty(), []);
    A.assertEquals(AR.combine(a1)(a2), [1, 2, 3, 4]);

    const a1a2_a3 = AR.combine(AR.combine(a1)(a2))(a3);
    const a1_a2a3 = AR.combine(a1)(AR.combine(a2)(a3));
    A.assertEquals(a1a2_a3, a1_a2a3);
  });
  //
  await ta.step("fold", () => {
    const sumArr = AR.foldl(Fn.add)(0);
    A.assertEquals(sumArr([1, 2, 3, 4]), 10);

    const decr100Arr = AR.foldr(Fn.substract)(100);
    A.assertEquals(decr100Arr([1, 2, 3, 4]), 90);

    /** @example
     * [1,2,3,4].reduce((s,n)=> s+n, ">")
     * ">1234"
     * foldl<number,string>((s) => (n) => s + n) (">") ([1,2,3,4])
     * ">1234"
     */
    const chainl = AR.foldl<number, string>((s) => (n) => s + n)(">");
    A.assertEquals(chainl([1, 2, 3, 4]), ">1234");

    /** @example
     * [1,2,3,4].reduceRight((s,n)=> s+n, "<")
     * "<4321"
     * foldr<number,string>((n) => (s) => s + n) ("<") ([1,2,3,4])
     * "<4321"
     */
    const chainr = AR.foldr<number, string>((n) => (s) => s + n)("<");
    A.assertEquals(chainr([1, 2, 3, 4]), "<4321");
  });

  await ta.step("fold monoid - or using the same operator...", () => {
    const mfoldl = AR.foldl(AR.combine)(AR.empty());
    const mfoldr = AR.foldr(AR.combine)(AR.empty());
    const a14 = [1, 2, 3, 4];
    const aa14 = a14.map((a) => [a]);
    A.assertEquals(mfoldl(aa14), a14);
    A.assertEquals(mfoldr(aa14), a14);
  });
});
